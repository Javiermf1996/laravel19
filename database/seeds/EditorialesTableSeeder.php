<?php

use Illuminate\Database\Seeder;
use App\Editorial;
class EditorialesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Editorial::create(['name' => 'Planeta']);
        Editorial::create(['name' => 'SM']);
        Editorial::create(['name' => 'ANAYA']);
        Editorial::create(['name' => 'Santillana']);
         
    }
}
