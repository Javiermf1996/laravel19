<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlacesTableSeeder::class);
        $this->call(EditorialesTableSeeder::class);
        $this->call(CdusTableSeeder::class);
        $this->call(BooksTableSeeder::class);
  
 
    }
}
