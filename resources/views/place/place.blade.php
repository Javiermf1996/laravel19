<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
<a href="/places/create">Nuevo</a>
    @forelse($places as $place)
    <tr>
        <td>{{$place->name}}</td>
        <td>{{count($place->books)}}</td>
        <td><a href="/places/{{$place->id}}">Ver</a></td>
        <td><a href="/places/{{$place->id}}/edit">Editar</a></td>
        <td>
        <form action="/places/{{$place->id}}" method="post">
        @csrf
        <input type="hidden" name="_method" value="delete">
        <input type="submit" value="delete">
        </td>
    </form>
    @empty
        <tr><td>No hay places</td></tr>
        @endforelse
    </tr>
    </table>
    <table>
    {{$places->render()}}
    </table>
</body>
</html>