<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Hola mundo</h1>
    <p>{{$saludo}}</p>
    <ul>
    @forelse($greetings as $greeting)
        <li>{{$greeting}}</li>
    @empty
        No hay saludos
        @endforelse
    
    </ul>
</body>
</html>