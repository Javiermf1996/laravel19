@extends('layouts.app')

@section('content')
    <h1>Hola libros del mundo</h1>
    <a href="/books/create">Nuevo</a>

    El ultimo libro es:
    @if(Session::get('lastbook'))
    {{Session::get('lastbook')->title}}
    {{Session::get('lastbook')->editorial->name}}
    <a href="/books/forget">Olvida el libro</a>
    @else
        Ninguno
    @endif

    <br>
    Los libros visitados son:
    @if(Session::get('books'))
    @foreach(Session::get('books') as $index => $book)
        <li>{{$index}} - {{$book->title}}</li>
        @endforeach
    @else
        No has visto ninguno todavia
    @endif
    <table>
    <tr>
        <th>Titulo</th>
        <th>Ubicación</th>
        <th>Editorial</th>
        <th>CDU</th>
        <th>Acciones</th>
    </tr>
    @forelse($books as $book)
    <tr>
        <td>{{$book->title}}</td>
        <td>{{$book->place ? $book->place->name : ''}}</td>
        <td>{{$book->editorial->name}}</td>
        <td>{{$book->Cdu->description}}</td>
        @can('view', $book)
        <td><a href="/books/{{$book->id}}">Ver</a></td>
        @endcan
        <td><a href="/books/{{$book->id}}/edit">Editar</a></td>
        <td>
        <form action="/books/{{$book->id}}" method="post">
        @csrf
        <input type="hidden" name="_method" value="delete">
        <input type="submit" value="delete">
        </td>
    </form>
    @empty
        <tr><td>No hay libros</td></tr>
        @endforelse
    </tr>
    </table>
    <table>
    {{$books->render()}}
    </table>
@endsection