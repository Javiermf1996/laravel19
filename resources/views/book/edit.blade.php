<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Edición del libro</h1>

    <form action="/books/{{$book->id}}" method="post">
    @csrf
    <label for="title">Titulo</label>
    <input type="text" name="title" value="{{ $book->title}}"><br>
    <label for="title">Autor</label>
    <input type="text" name="author" value="{{ $book->author}}"><br>
    <label for="title">Place</label>
    <select name="place_id">
        @foreach($places as $place)
            <option value="{{$place->id}}" {{$place->id == $book->place_id ? 'selected="selected"' :'' }}>{{$place->name}}</option>

        @endforeach
        </select>
        <input type="hidden" name="_method" value="put">
        <input type="submit" value="edit">

</form>
    
</body>
</html>