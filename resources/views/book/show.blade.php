<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Detalle del libro {{$book->id}}</h1>

    <ul>

        <li>{{$book->title}}</li>
        <li>{{$book->author}}</li>
        <li>{{$book->place->name}}</li>
    </ul>
</body>
</html>