<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Books</title>
</head>
<body>
    <h1>Alta libro</h1>
    <form action="/books" method="post">
        <label for="title">Titulo</label>
        @csrf
        <input type="text" name="title" value="{{ old('title') }}">
        {{$errors->first('title')}}
        <br>
        <label for="title">Autor</label>
        <input type="text" name="author" value="{{ old('author') }}">
        {{$errors->first('author')}}
        <br>
        <label for="title">Place</label>
        <select name="place_id">
        @foreach($places as $place)
            <option value="{{$place->id}}"{{$place->id ==  old('place_id')  ? 'selected="selected"' :'' }}>{{$place->name}}</option>
        @endforeach
        </select>
        {{$errors->first('place_id')}}
        <br>
        <input type="submit" value="nuevo">
</form>

{{dd(Session::all())}}
</body>
</html>