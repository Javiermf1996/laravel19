<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title','author','cdu'];

    public function place(){
        return $this->belongsTo('App\Place');
    }
    public function editorial(){
        return $this->belongsTo('App\Editorial');
    }

    public function cdu(){
        return $this->belongsTo('App\Cdu','cdu','cdu');
    }
}
