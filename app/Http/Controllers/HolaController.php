<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HolaController extends Controller
{
   public function holamundo (){
       $saludo = 'que tal?';
       $greetings = array('Hell','BONJOUR');
       $greetings= [];
       return view('hola',['saludo' => $saludo, 'greetings' => $greetings]);
   }
}
