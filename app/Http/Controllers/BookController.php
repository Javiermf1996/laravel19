<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Place;
use App\Auth;

class BookController extends Controller
{
    public function index(){
        $books = Book::paginate(10);
        $this->authorize('viewAny', Book::class);
        return view('book.book',['books' =>$books]);
         
    }

    public function show($id,Request $request){
        
       // $user = \Auth::user();
        $book = Book::find($id);
        
        /*if(!$user->can('view',$book)){
            return "Alto";
        }*/
        //$book = Book::findOrFail($id);
        $request->session()->put('lastbook',$book);
        $books =$request->session()->get('books');
        $this->authorize('view',$book);
        if(!$books){
            $books = array();
        }
        $books[] = $book;
        $request->session()->put('books',$books);
        return view('book.show', ['book' => $book]);
    }

    public function create(){
        $places = Place::all();
        return view('book.create',['places' => $places]);
    }
    public function destroy($id){
        $book = Book::find($id);
        $book->delete();

        //Book::destroy(array(2,3,4));
        //return redirect('books');
        return back();
    }

    public function store(Request $request){

       // dd($request->all());
        //dd($request->input_title('title))
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'author' => 'required|max:255',
            'place_id' => 'required|exists:places,id',
        ]);
        dd($validatedData);
        $book = new Book;
        $book->title = $request->title;
        $book->author = $request->author;
        $book->place_id = $request->place_id;
        $book->save();

        return redirect('/books');
    }
    public function forget(Request $request){
        $request->session()->forget('lastbook');
        $request->session()->forget('books');
        return back();
    }
  
    public function edit($id)
    {
        $places = Place::all();
        $book = Book::find($id);
        return view('book.edit', ['book' => $book],['places' => $places]);
    }
    public function update($id,Request $request){
        $book = Book::find($id);

        $book->title = $request->title;
        $book->place_id = $request->place_id;
        //metodo fill para rellenar atributos a lo bestia
       // $book->fill($request->all());
        $book->save();
        return redirect('/books/' . $book->id);
    }

    
}
