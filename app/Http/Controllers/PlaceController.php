<?php

namespace App\Http\Controllers;
use App\Book;
use App\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $places = Place::paginate(10);
        return view('place.place',['places' =>$places]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('place.create');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $place = new Place;
        $place->name = $request->name;
        $place->save();

        return redirect('/places');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function showJson($id)
    {
        //$place = Place::with('books.editorial')->where('id','<',3)->orderBy('name','desc')->get();
        $place = Place::with('books.editorial')->find($id);
        $books = Book::all(); 
       // $place = Place::find($id);
        //$place = place::findOrFail($id);
        return view('place.show', ['place' => $place],['books' =>$books]); buena
      // return $place;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $place)
    {
       // $place = Place::find($id);
        return view('place.edit', ['place' => $place]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255|unique:places,name,'.$place->id,
        ]);
       // $place = Place::find($id);
        $place->name = $request->name;
        //metodo fill para rellenar atributos a lo bestia
       // $place->fill($request->all());
        $place->save();
        return redirect('/places/' . $place->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $place)
    {
      //  $place = Place::find($id);
        $place->delete();

        //place::destroy(array(2,3,4));
        //return redirect('places');
        return back();
    }
}
