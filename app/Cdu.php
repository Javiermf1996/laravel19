<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cdu extends Model
{
    protected $fillable = ['cdu' , 'description'];
}
